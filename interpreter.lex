//Specification JFlex
import java_cup.runtime.Symbol;

%%
%unicode
%cup
%line
%column

%{
public int getYyLine(){return yyline+1;}
public int getYyColumn(){return yycolumn+1;}
public String getYyText(){return yytext();}
%}

%%

"vrai"        {return new Symbol(sym.VRAI,yytext());}
"faux"        {return new Symbol(sym.FAUX,yytext());}
"("           {return new Symbol(sym.POUV,yytext());}
")"           {return new Symbol(sym.PFER,yytext());}
"non"         {return new Symbol(sym.NONL,yytext());}
"ou"          {return new Symbol(sym.OUL,yytext());}
"et"          {return new Symbol(sym.ETL,yytext());}
"="           {return new Symbol(sym.AFF,yytext());}
"PRINT"       {return new Symbol(sym.PRINT,yytext());}
[a-zA-Z]+     {return new Symbol(sym.VAR,yytext());}
\n            {return new Symbol(sym.RL,yytext());}
[^" "\t\r]    {System.out.println("ERREUR Lexical : ligne:"+yyline+" colonne:"+yycolumn+" '"+yytext()+"' non reconnu !");}
.             {}
